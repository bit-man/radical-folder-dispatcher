#!/usr/bin/env bash

testFolder=`dirname $0`
source $testFolder/../rfs-install

function folderA() {
  [[ $1 =~ aaa\/a ]] && return 1
  return 0
}

function testFolderFound() {
  rfsConfigFile=$testFolder/config
  command=`rfs_main aaa/a`
  assertEquals "functionA" $command
}

function testFolderNotFound() {
  rfsConfigFile=$testFolder/config
  rfsConfigEnd=$testFolder/end
  command=`rfs_main aaa/b`
  assertEquals "ABCDE"  "$command"
}



# Load shUnit2.
source shunit2
