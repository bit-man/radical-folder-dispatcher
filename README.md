# radical-folder-dispatcher

Dispatch an event on folder access.

Let's say that upon entering on your shiny new project located at folder `/home/me/project` compilation is fired
automatically by calling `gradle build`

Prerequisites
---

bash 3.2 or later. It might run in previous version but haven't been tested.

Install
---

Create config folder

    # mkdir -p $HOME/.rfs

Inside it create config file that will fire `IsThisTheRightFolder` function each time it reaches a folder and if this function returns 0 will execute program `/home/me/compile-my-project`

    # cat <<EOF >$HOME/.rfs/config
    IsThisTheRightFolder|/home/me/compile-my-project
    EOF

Create function `IsThisTheRightFolder`

    # function IsThisTheRightFolder() {
          [[ $1 =~ \/home\/me\/project ]] && return 0
          return 1
    }

Create compile script

    # cat <<EOF >$HOME/compile-my-project
    #!/usr/bin/env bash

    gradle build
    EOF
    # chmod chmod u+rwx $HOME/compile-my-project


Say good bye when no matching folder

    # cat <<EOF >$HOME/.rfs/end
    /home/me/good-bye
    EOF

Also create `good-bye` script

    # cat <<EOF >$HOME/good-bye
    #!/usr/bin/env bash

    echo "Bye bye life, bye bye happiness"
    EOF
    # chmod chmod u+rwx $HOME/good-bye

Install `radical-folder-dispatcher`

    # source ./rfs-Install

Change to your project folder and see how it is build

    # cd $HOME/project

    ....   
    BUILD SUCCESSFUL

    Total time: 3.077 secs
    # cd
    Bye bye life, bye bye happiness
    #
